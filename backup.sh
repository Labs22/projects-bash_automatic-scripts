#!/bin/bash
#function: backup
#Autor: Luan -Barbosa

if [ "$UID" != "0" ]; then
   echo "run it with admin privileges." 
   exit
fi
 
while sleep 1d
do
   echo "criando backup $(date +\d\m\Y)..."
   tar -cz /home >/home$(date +\d\m\Y).tar.gz
done